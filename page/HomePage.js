let HomePage = function () {
    let firstNumber_input = element(by.model('first'));
    let secondNumber_input = element(by.model('second'));
    let goButton = element(by.css('[ng-click="doAddition()"]'));
    let selectOperationMULTIPLICATION = element(by.xpath(".//option[@value='MULTIPLICATION']"));
    let selectOperationADDITION = element(by.xpath(".//option[@value='ADDITION']"));
    let selectOperationSUBTRACTION = element(by.xpath(".//option[@value='SUBTRACTION']"));
    let selectOperationDIVISION = element(by.xpath(".//option[@value='DIVISION']"));
    let operator = element(by.model('operator'));

    this.getUrlProject = function (url) {
        browser.get(url);
    }

    this.enterFirstValueNumber = function (firstValueNumber) {
        firstNumber_input.sendKeys(firstValueNumber);
    }

    this.enterSecondValueNumber = function (secondValueNumber) {
        secondNumber_input.sendKeys(secondValueNumber);
    }

    this.selectOperation = function (key) {
        switch (key) {
            case "MULTIPLICATION":
                selectOperationMULTIPLICATION.click();
                break;
            case "ADDITION":
                selectOperationADDITION.click();
                break;
            case "SUBTRACTION":
                selectOperationSUBTRACTION.click();
                break;
            case "DIVISION":
                selectOperationDIVISION.click();
                break;
            default:
                break;
        }
    }

    this.operationIcon = function () {
        operator.click();
    }

    this.clickButtonGo = function () {
        goButton.click();
    }

    this.verifyResultTitle = function (resultTitle) {
        expect(resultTitle).toEqual('Super Calculator');
    }

    this.verifyResult = function (result) {
        let output = element(by.binding('latest'));
        expect(output.getText()).toEqual(result);
    }
};
module.exports = new HomePage();