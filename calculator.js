
const homePage = require('./page/HomePage');
describe('Inicializar Calculator Angular Demo', function() {
    const urlProject = {
        url: 'http://juliemr.github.io/protractor-demo/'        
    };

    it('should have a title', function() {
      browser.get(urlProject.url);      
      expect(browser.getTitle()).toEqual('Super Calculator');
      browser.sleep(2000);
    });
    
    it('It should have add', () => {
        homePage.getUrlProject(urlProject.url);
        homePage.enterFirstValueNumber('10');
        homePage.enterSecondValueNumber('13');
        homePage.clickButtonGo();
        browser.sleep(2000);
    });
    
    it('should multiply two integers', () => {
        homePage.getUrlProject(urlProject.url);
        homePage.enterFirstValueNumber('10');
        homePage.enterSecondValueNumber('13');
        homePage.operationIcon();
        homePage.selectOperation("MULTIPLICATION");
        homePage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('130');

        homePage.enterFirstValueNumber('10');
        homePage.enterSecondValueNumber('13');
        homePage.operationIcon();
        homePage.selectOperation("ADDITION");
        homePage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('23');

        homePage.enterFirstValueNumber('10');
        homePage.enterSecondValueNumber('13');
        homePage.operationIcon();
        homePage.selectOperation("SUBTRACTION");
        homePage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('-3');

        homePage.enterFirstValueNumber('2');
        homePage.enterSecondValueNumber('10');
        homePage.operationIcon();
        homePage.selectOperation("DIVISION");
        homePage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('0.2');
        browser.sleep(2000);
    });

  });